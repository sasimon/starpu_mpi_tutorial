#include <stdio.h>
#include <starpu.h>
#include <starpu_mpi.h>

#define NX 32
#define NY 5
#define block_size 4
#define nb_handle 6
#define interior 0
#define full 1
#define left 2
#define right 3
#define left_overlap 4
#define right_overlap 5
#define OVERLAP 1

#define CHECK_RETURN(val, str) {if (val == -ENODEV) goto enodev; STARPU_CHECK_RETURN_VALUE(val, str);}
#define NAME_MAX_LENGTH 128

// Boundary condition periodic
// Each block contains the overlaps it needs. These overlaps are updated with
// StarPU task

void empty(void *buffers[], void *cl_arg)
{
	/* This doesn't need to do anything, it's simply used to make coherency
	 * between the two views, by simply running on the home node of the
	 * data, thus getting back all data pieces there.  */
	(void)buffers;
	(void)cl_arg;
}

struct starpu_codelet cl_switch =
{
	.cpu_funcs = {empty},
	.nbuffers = STARPU_VARIABLE_NBUFFERS,
	.name = "switch"
};

void block_fill_m1(void* buffers[], void* cl_arg)
{
  unsigned i, j;
  unsigned nx = STARPU_MATRIX_GET_NX(buffers[0]);
  unsigned ny = STARPU_MATRIX_GET_NY(buffers[0]);
  unsigned ld = STARPU_MATRIX_GET_LD(buffers[0]);
  int *val = (int *)STARPU_MATRIX_GET_PTR(buffers[0]);

  for(j=0; j < ny; j++)
    for(i=0; i < nx; i++)
      val[i + j*ld] = -1;

}

struct starpu_codelet cl_fill_m1 =
{
  .cpu_funcs = {block_fill_m1},
  .cpu_funcs_name = {"block_fill_m1"},
  .nbuffers = 1,
  .modes = {STARPU_W},
  .name = "block_fill_m1"
};
void block_fill(void* buffers[], void* cl_arg)
{
  unsigned i, j;
  unsigned xoffset;
  unsigned nx = STARPU_MATRIX_GET_NX(buffers[1]);
  unsigned ny = STARPU_MATRIX_GET_NY(buffers[1]);
  unsigned ld = STARPU_MATRIX_GET_LD(buffers[1]);
  int *val = (int *)STARPU_MATRIX_GET_PTR(buffers[1]);

  starpu_codelet_unpack_args(cl_arg, &xoffset);
  for(j=0; j < ny; j++)
    for(i=0; i < nx; i++)
      val[i + j*ld] = (i+xoffset) + j*NX;

}

struct starpu_codelet cl_fill =
{
  .cpu_funcs = {block_fill},
  .cpu_funcs_name = {"block_fill"},
  .nbuffers = 2,
  .modes = {STARPU_R, STARPU_W},
  .name = "block_fill"
};

void dump(void* buffers[], void* cl_arg)
{
  unsigned i, j;
  int num;
  char fname[NAME_MAX_LENGTH];
  unsigned nx = STARPU_MATRIX_GET_NX(buffers[0]);
  unsigned ny = STARPU_MATRIX_GET_NY(buffers[0]);
  unsigned ld = STARPU_MATRIX_GET_LD(buffers[0]);
  int *val = (int *)STARPU_MATRIX_GET_PTR(buffers[0]);

  starpu_codelet_unpack_args(cl_arg, &num);

  sprintf(fname, "A%d.txt", num);
  
  FILE* f = fopen(fname, "w");
  for(j=0; j < ny; j++){
    for(i=0; i < nx; i++)
      fprintf(f, "%03d  ", val[i + j*ld]);
    fprintf(f, "\n");
  }
  fclose(f);

}

struct starpu_codelet cl_dump=
{
  .cpu_funcs = {dump},
  .cpu_funcs_name = {"dump"},
  .nbuffers = 1,
  .modes = {STARPU_R},
  .name = "dump"
};

void dump_blocks(int** blocks, char* fname, int bs, int be)
{
  int b, i, j;
  int nb_block = NX/block_size;
  int block_size_alloc = block_size + 2*OVERLAP;
  FILE* f = fopen(fname, "w");
  for (b = bs; b < be; b++){
    fprintf(f, "Block %d\n", b);
    for(j=0; j < NY; j++){
      for(i=0; i < block_size_alloc; i++)
        fprintf(f, "%03d  ", blocks[b][i + j*block_size_alloc]);
      fprintf(f, "\n");
    }
    fprintf(f, "\n");
  }
  fclose(f);

}

void copy_block(void* buffers[], void* cl_arg)
{
  int i, j;
  unsigned nxa = STARPU_MATRIX_GET_NX(buffers[0]);
  unsigned nya = STARPU_MATRIX_GET_NY(buffers[0]);
  unsigned lda = STARPU_MATRIX_GET_LD(buffers[0]);
  int *block_A = (int *)STARPU_MATRIX_GET_PTR(buffers[0]);
  unsigned nxb = STARPU_MATRIX_GET_NX(buffers[1]);
  unsigned nyb = STARPU_MATRIX_GET_NY(buffers[1]);
  unsigned ldb = STARPU_MATRIX_GET_LD(buffers[1]);
  int *block_B = (int *)STARPU_MATRIX_GET_PTR(buffers[1]);

  STARPU_ASSERT(nxa == nxb);
  STARPU_ASSERT(nya == nyb);
  //printf("nx %d ny %d ld_block %d ld_A %d block[0] %d block[1] %d\n", nxa, nya, lda, ldb, block_A[0], block_A[1]);
  for(j=0; j < nya; j++)
    for(i=0; i < nxb; i++)
      block_B[i + j*ldb] = block_A[i + j*lda];
}

struct starpu_codelet cl_copy_block=
{
  .cpu_funcs = {copy_block},
  .cpu_funcs_name = {"copy_block"},
  .nbuffers = 2,
  .modes = {STARPU_R, STARPU_W},
  .name = "copy_block"
};

void block_compute_interior(void* buffers[], void* cl_arg)
{
  int i, j;
  unsigned nx = STARPU_MATRIX_GET_NX(buffers[0]);
  unsigned ny = STARPU_MATRIX_GET_NY(buffers[0]);
  unsigned ld = STARPU_MATRIX_GET_LD(buffers[0]);
  unsigned ldb = STARPU_MATRIX_GET_LD(buffers[1]);
  int *block_A = (int *)STARPU_MATRIX_GET_PTR(buffers[0]);
  int *block_B = (int *)STARPU_MATRIX_GET_PTR(buffers[1]);

  STARPU_ASSERT(ld == ldb);

  // Block interior
  for(j=0; j < ny; j++)
    for(i=1; i < nx-1; i++)
      block_B[i + j*ld] = block_A[i - 1 + j*ld] + block_A[i + j*ld] + block_A[i + 1 + j*ld];

  // Left boundary only contribution from the right
  i = 0;
  for(j=0; j < ny; j++)
    block_B[i + j*ld] = block_A[i + j*ld] + block_A[i + 1 + j*ld];

  // Right boundary only contribution from the left
  i = block_size-1;
  for(j=0; j < ny; j++)
    block_B[i + j*ld] = block_A[i - 1 + j*ld] + block_A[i + j*ld];
}

struct starpu_codelet cl_compute_interior=
{
  .cpu_funcs = {block_compute_interior},
  .cpu_funcs_name = {"block_compute_interior"},
  .nbuffers = 2,
  .modes = {STARPU_R, STARPU_W},
  .name = "block_compute_interior"
};

void compute_boundaries(void* buffers[], void* cl_arg)
{
  int i, j;
  unsigned nx = STARPU_MATRIX_GET_NX(buffers[0]);
  unsigned ny = STARPU_MATRIX_GET_NY(buffers[0]);
  unsigned ld = STARPU_MATRIX_GET_LD(buffers[0]);
  int *block_B = (int *)STARPU_MATRIX_GET_PTR(buffers[0]);

  // Left boundary
  i = OVERLAP;
  for(j=0; j < ny; j++)
    block_B[i + j*ld] = block_B[i - 1 + j*ld] + block_B[i + j*ld];

  // Right boundary
  i = nx-1-OVERLAP;
  for(j=0; j < ny; j++)
    block_B[i + j*ld] = block_B[i + j*ld] + block_B[i+1 + j*ld];
}

struct starpu_codelet cl_compute_boundaries=
{
  .cpu_funcs = {compute_boundaries},
  .cpu_funcs_name = {"compute_boundaries"},
  .nbuffers = 1,
  .modes = {STARPU_RW},
  .name = "compute_boundaries"
};

int rank_of_block(int b, int nb_block, int nb_ranks)
{
  int block_per_rank = nb_block / nb_ranks;
  return b/block_per_rank;
}

int is_neighbouring_block(int b, int nb_block, int nb_ranks)
{
  // All ranks have two blocks neighbourg of other ranks, the first and the last ones
  int block_per_rank = nb_block / nb_ranks;
  return (b%block_per_rank==0) || ((b+1)%block_per_rank==0);

}


void alloc_declare_blocks(int*** blocks_return, starpu_data_handle_t ** blocks_handle_return, int bs, int be, int nb_block, int handle, int nx, int ny, int overlap, int rank, int nb_ranks, int tag_offset)
{
  int**  blocks;
  starpu_data_handle_t* blocks_handle;
  int b, h;
  int block_rank;
  int elt_size = sizeof(int);
  int nx_alloc = nx + 2*overlap;

  *blocks_handle_return = (starpu_data_handle_t*) malloc(nb_block * handle * sizeof(starpu_data_handle_t));
  *blocks_return = (int**) malloc(nb_block * sizeof(int*));
  blocks = *blocks_return;
  blocks_handle = *blocks_handle_return;

  // Each rank allocates and declares only data it will compute
  for (b = 0; b < nb_block; b++)
  {
    block_rank = rank_of_block(b, nb_block, nb_ranks);
    if(rank == block_rank) 
    {
      blocks[b] = (int*) malloc(nx_alloc * ny * sizeof(int)); 
      starpu_matrix_data_register(&blocks_handle[full          + b*handle], STARPU_MAIN_RAM, (uintptr_t)&blocks[b][0], nx_alloc, nx_alloc, ny, elt_size);
      starpu_matrix_data_register(&blocks_handle[interior      + b*handle], STARPU_MAIN_RAM, (uintptr_t)&blocks[b][overlap], nx_alloc, nx, ny, elt_size);
      starpu_matrix_data_register(&blocks_handle[left          + b*handle], STARPU_MAIN_RAM, (uintptr_t)&blocks[b][overlap], nx_alloc, overlap, ny, elt_size);
      starpu_matrix_data_register(&blocks_handle[right         + b*handle], STARPU_MAIN_RAM, (uintptr_t)&blocks[b][nx_alloc-1-overlap], nx_alloc, overlap, ny, elt_size);
      starpu_matrix_data_register(&blocks_handle[left_overlap  + b*handle], STARPU_MAIN_RAM, (uintptr_t)&blocks[b][0], nx_alloc, overlap, ny, elt_size);
      starpu_matrix_data_register(&blocks_handle[right_overlap + b*handle], STARPU_MAIN_RAM, (uintptr_t)&blocks[b][nx_alloc-1-overlap+1], nx_alloc, overlap, ny, elt_size);
      for (h=0 ; h < handle; h++)
      {
        printf("Registering to MPI block %d handle %d (0x%x) with tag %d on rank %d\n", b, h, blocks_handle[h + b*handle], tag_offset + h + b*handle, rank);
        starpu_mpi_data_register(blocks_handle[h + b*handle], tag_offset + h + b*handle, block_rank);
      }
    }
    else if(is_neighbouring_block(b, nb_block, nb_ranks) || rank == 0)//data handles registered on rank 0 for data gathering operation during IO
    {
      blocks[b] = NULL; 
      starpu_matrix_data_register(&blocks_handle[full          + b*handle], -1, (uintptr_t)NULL, nx_alloc, nx_alloc, ny, elt_size);
      starpu_matrix_data_register(&blocks_handle[interior      + b*handle], -1, (uintptr_t)NULL, nx_alloc, nx, ny, elt_size);
      starpu_matrix_data_register(&blocks_handle[left          + b*handle], -1, (uintptr_t)NULL, nx_alloc, overlap, ny, elt_size);
      starpu_matrix_data_register(&blocks_handle[right         + b*handle], -1, (uintptr_t)NULL, nx_alloc, overlap, ny, elt_size);
      starpu_matrix_data_register(&blocks_handle[left_overlap  + b*handle], -1, (uintptr_t)NULL, nx_alloc, overlap, ny, elt_size);
      starpu_matrix_data_register(&blocks_handle[right_overlap + b*handle], -1, (uintptr_t)NULL, nx_alloc, overlap, ny, elt_size);
      for (h=0 ; h < handle; h++)
      {
        printf("Registering to MPI, block %d handle %d (0x%x) with tag %d on rank %d as a boundary on rank %d\n", b, h, blocks_handle[h + b*handle], tag_offset + h + b*handle, block_rank, rank);
        starpu_mpi_data_register(blocks_handle[h + b*handle], tag_offset + h + b*handle, block_rank);
      }
    }
    else
    {
        printf("Doing nothing for block %d on rank %d\n", b, rank);
        blocks[b] = NULL; 
        for (h=0 ; h < handle; h++)
          blocks_handle[h + b*handle] = NULL;
    }
  }
}

int main(int argc, char** argv)
{
  int A[NX*NY], B[NX*NY];
  int** blocks_A, **blocks_B;
  int ret, nb_block, b, i, j, xoffset;
  int rank, size, tag_offset, blocks_per_rank, bs, be;
  int num=0;
  char fname[NAME_MAX_LENGTH];
  starpu_data_handle_t *blocks_A_handle, *blocks_B_handle;
  starpu_data_handle_t *A_handle, *B_handle;
  starpu_data_handle_t A_large_handle, B_large_handle;
  struct starpu_data_descr *A_descr, *B_descr;

  STARPU_ASSERT((NX%block_size) == 0);
  nb_block = NX/block_size;
  STARPU_ASSERT((nb_block) >= 8);

  ret = starpu_mpi_init_conf(&argc, &argv, 1, MPI_COMM_WORLD, NULL);
  if (ret == -ENODEV)
    return 77;
  STARPU_CHECK_RETURN_VALUE(ret, "starpu_init");

  starpu_mpi_comm_rank(MPI_COMM_WORLD, &rank);
  starpu_mpi_comm_size(MPI_COMM_WORLD, &size);
  
  STARPU_ASSERT((nb_block%size) == 0);
  blocks_per_rank = nb_block / size;
  bs = rank*blocks_per_rank;
  be = (rank+1)*blocks_per_rank;

	/* force to execute task on the home_node, here it is STARPU_MAIN_RAM */
	cl_switch.specific_nodes = 1;
	for (i = 0; i < STARPU_NMAXBUFS; i++)
		cl_switch.nodes[i] = STARPU_MAIN_RAM;

  /***************************************************************************/
  /*********     Allocating and declaring to StarPU blocks for A & B *********/
  /***************************************************************************/
  printf("Allocating A & B on rank %d\n", rank);
  tag_offset = 0;
  alloc_declare_blocks(&blocks_A, &blocks_A_handle, bs, be, nb_block, nb_handle, block_size, NY, OVERLAP, rank, size, tag_offset);
  tag_offset += nb_block*nb_handle;
  alloc_declare_blocks(&blocks_B, &blocks_B_handle, bs, be, nb_block, nb_handle, block_size, NY, OVERLAP, rank, size, tag_offset);
  tag_offset += nb_block*nb_handle;

  printf("Filling A with -1 on rank %d\n", rank);
  /***************************************************************************/
  /*********     Data intialisation                                  *********/
  /***************************************************************************/
  /* Fill the full blocks with -1  */
  for (b = bs; b < be; b++)
  {
    printf("fill_m1 for b %d rank %d handle 0x%x\n", b, rank, blocks_A_handle[full + b*nb_handle]);
    ret = starpu_mpi_task_insert(MPI_COMM_WORLD, &cl_fill_m1, 
        STARPU_W, blocks_A_handle[full + b*nb_handle], 
        0);
    CHECK_RETURN(ret,"starpu_task_submit")
  }

  starpu_task_wait_for_all();
  sprintf(fname, "A0_blocks_%d.txt", rank);
  dump_blocks(blocks_A, fname, bs, be);
  printf("Init A on rank %d\n", rank);

  /* Fill the interior blocks with real values  */
  for (b = bs; b < be; b++)
  {
    xoffset = b * block_size;
    ret = starpu_mpi_task_insert(MPI_COMM_WORLD, &cl_fill, 
        STARPU_R, blocks_A_handle[full + b*nb_handle], 
        STARPU_W, blocks_A_handle[interior + b*nb_handle], 
        STARPU_VALUE, &xoffset, sizeof(xoffset),
        0);
    CHECK_RETURN(ret,"starpu_task_submit")
  }
  starpu_task_wait_for_all();
  sprintf(fname, "A0p_blocks_%d.txt", rank);
  dump_blocks(blocks_A, fname, bs, be);

  starpu_task_wait_for_all();
  /***************************************************************************/
  /*********                    Dump initial data                *************/
  /***************************************************************************/
  printf("Writing A on rank %d\n", rank);

  // Fetching data from all ranks
  A_handle = (starpu_data_handle_t*) malloc(nb_block * sizeof(starpu_data_handle_t));
  for (b = 0; b < nb_block; b++)
  {
    printf("submitting task &cl_copy_block for block %d\n", b);
    if(rank ==0)
      starpu_matrix_data_register(&A_handle[b], STARPU_MAIN_RAM, (uintptr_t)&A[b*block_size], NX, block_size, NY, sizeof(int));
    else
      starpu_matrix_data_register(&A_handle[b], -1, (uintptr_t)NULL, NX, block_size, NY, sizeof(int));

    starpu_mpi_data_register(A_handle[b], b + tag_offset, 0);
    ret = starpu_mpi_task_insert(MPI_COMM_WORLD, &cl_copy_block, 
        STARPU_R, blocks_A_handle[interior + b*nb_handle], 
        STARPU_W, A_handle[b], 
        0);
    CHECK_RETURN(ret,"starpu_task_submit")
  }
  tag_offset += nb_block;

  if(rank == 0) 
  {
    /* Synchronize data handle pointing on parts of A with the global array  */
    starpu_matrix_data_register(&A_large_handle, STARPU_MAIN_RAM, (uintptr_t)&A[0], NX, NX, NY, sizeof(int));
    A_descr = malloc(nb_block * sizeof(struct starpu_data_descr));
    for (b = 0; b < nb_block; b++)
    {
      A_descr[b].handle = A_handle[b];
      A_descr[b].mode = STARPU_RW;
    }
    ret = starpu_task_insert(&cl_switch, STARPU_DATA_MODE_ARRAY, A_descr, nb_block, STARPU_W, A_large_handle, 0);
    CHECK_RETURN(ret,"starpu_task_submit")

    ret = starpu_task_insert(&cl_dump, STARPU_R, A_large_handle, STARPU_VALUE, &num, sizeof(num), 0);
    CHECK_RETURN(ret,"starpu_task_submit")
    free(A_descr);
  }
  printf("Computing B on rank %d\n", rank);

  
  /***************************************************************************/
  /*********              Do the computation                     *************/
  /***************************************************************************/
  for (b = bs; b < be; b++)
  {
    // Task computing the interior
    ret = starpu_mpi_task_insert(MPI_COMM_WORLD, &cl_compute_interior, 
        STARPU_R, blocks_A_handle[interior + b*nb_handle], 
        STARPU_W, blocks_B_handle[interior + b*nb_handle], 
        0);
    CHECK_RETURN(ret,"starpu_task_submit")

    // Tasks updating ghosts receiving tasks
    int l, r;
    l   = (b - 1 + nb_block)%nb_block;
    r  = (b + 1)%nb_block;
    ret = starpu_mpi_task_insert(MPI_COMM_WORLD, &cl_copy_block, 
        STARPU_R, blocks_A_handle[right + l*nb_handle], 
        STARPU_W, blocks_B_handle[left_overlap + b*nb_handle], 
        0);
    CHECK_RETURN(ret,"starpu_task_submit")
    printf("rank %d receiving left ghost l=%d b=%d handle from 0x%x handle to 0x%x\n", rank, l, b, blocks_A_handle[right + l*nb_handle], blocks_B_handle[left_overlap + b*nb_handle]);

    ret = starpu_mpi_task_insert(MPI_COMM_WORLD, &cl_copy_block, 
        STARPU_R, blocks_A_handle[left + r*nb_handle], 
        STARPU_W, blocks_B_handle[right_overlap + b*nb_handle], 
        0);
    CHECK_RETURN(ret,"starpu_task_submit")
    printf("rank %d receiving right ghost r=%d b=%d handle from 0x%x handle to 0x%x\n", rank, r, b, blocks_A_handle[left + r*nb_handle], blocks_B_handle[right_overlap + b*nb_handle]);

    // Tasks updating ghosts sending tasks
    ret = starpu_mpi_task_insert(MPI_COMM_WORLD, &cl_copy_block, 
        STARPU_R, blocks_A_handle[right + b*nb_handle], 
        STARPU_W, blocks_B_handle[left_overlap + r*nb_handle], 
        0);
    CHECK_RETURN(ret,"starpu_task_submit")
    printf("rank %d sending right ghost l=%d b=%d handle from 0x%x handle to 0x%x\n", rank, l, b, blocks_A_handle[right + b*nb_handle], blocks_B_handle[left_overlap + r*nb_handle]);

    ret = starpu_mpi_task_insert(MPI_COMM_WORLD, &cl_copy_block, 
        STARPU_R, blocks_A_handle[left + b*nb_handle], 
        STARPU_W, blocks_B_handle[right_overlap + l*nb_handle], 
        0);
    CHECK_RETURN(ret,"starpu_task_submit")
    printf("rank %d sending left ghost r=%d b=%d handle from 0x%x handle to 0x%x\n", rank, r, b, blocks_A_handle[left + b*nb_handle], blocks_B_handle[right_overlap + l*nb_handle]);
    
    
    // Synchronising data. The full block is updated with values contained in small handles
    ret = starpu_task_insert(&cl_switch, 
        STARPU_RW, blocks_B_handle[right_overlap + b*nb_handle],
        STARPU_RW, blocks_B_handle[left_overlap  + b*nb_handle],
        STARPU_RW, blocks_B_handle[interior      + b*nb_handle],
        STARPU_W,  blocks_B_handle[full          + b*nb_handle], 
        0);
    CHECK_RETURN(ret,"starpu_task_submit")

    // Task computing boundaries thanks to updated ghosts
    ret = starpu_mpi_task_insert(MPI_COMM_WORLD, &cl_compute_boundaries, 
        STARPU_RW, blocks_B_handle[full + b*nb_handle], 
        0);
    CHECK_RETURN(ret,"starpu_task_submit")

  }


  /* Copy the blocks in the large array B for output */
  starpu_task_wait_for_all();
  
  /**************************************************************************/
  /********                    Dump final data                  *************/
  /**************************************************************************/
  printf("Writing B on rank %d\n", rank);
  sprintf(fname, "A1_blocks_%d.txt", rank);
  dump_blocks(blocks_B, fname, bs, be);


  // Fetching data from all ranks
  B_handle = (starpu_data_handle_t*) malloc(nb_block * sizeof(starpu_data_handle_t));
  for (b = 0; b < nb_block; b++)
  {
    printf("submitting task &cl_copy_block for block %d on rank %d\n", b, rank);
    if(rank ==0)
      starpu_matrix_data_register(&B_handle[b], STARPU_MAIN_RAM, (uintptr_t)&B[b*block_size], NX, block_size, NY, sizeof(int));
    else
      starpu_matrix_data_register(&B_handle[b], -1, (uintptr_t)NULL, NX, block_size, NY, sizeof(int));

    starpu_mpi_data_register(B_handle[b], b + tag_offset, 0);
    ret = starpu_mpi_task_insert(MPI_COMM_WORLD, &cl_copy_block, 
        STARPU_R, blocks_B_handle[interior + b*nb_handle], 
        STARPU_W, B_handle[b], 
        0);
    CHECK_RETURN(ret,"starpu_task_submit")
  }

  if(rank == 0) 
  {
    /* Synchronize data handle pointing on parts of A with the global array  */
    starpu_matrix_data_register(&B_large_handle, STARPU_MAIN_RAM, (uintptr_t)&B[0], NX, NX, NY, sizeof(int));
    B_descr = malloc(nb_block * sizeof(struct starpu_data_descr));
    for (b = 0; b < nb_block; b++)
    {
      B_descr[b].handle = B_handle[b];
      B_descr[b].mode = STARPU_RW;
    }
    printf("submitting task &cl_switch\n");
    ret = starpu_task_insert(&cl_switch, STARPU_DATA_MODE_ARRAY, B_descr, nb_block, STARPU_W, B_large_handle, 0);
    CHECK_RETURN(ret,"starpu_task_submit")

    printf("submitting task &cl_dump\n");
    num=1;
    ret = starpu_task_insert(&cl_dump, STARPU_R, B_large_handle, STARPU_VALUE, &num, sizeof(num), 0);
    CHECK_RETURN(ret,"starpu_task_submit")
    free(B_descr);
  }

  free(A_handle);
  free(B_handle);
  starpu_mpi_shutdown();

  return ret;

enodev:
  starpu_shutdown();
  return 77;

}
